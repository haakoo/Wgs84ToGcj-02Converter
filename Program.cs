﻿using System;
using System.IO;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Need to supply filename to convert");
                Environment.Exit(1);
            }

            foreach (var filename in args)
            {
                if (File.Exists(filename))
                    Wgs2GcjConverter.ConvertFile(filename);
            }

        }
    }
}
