using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using EvilTransform;

class Wgs2GcjConverter
{
    public static void ConvertFile(string filename)
    {
        if (filename.Contains("gcj"))
            return;
       
        var transformer = new Transform();
        XNamespace ns = "http://www.topografix.com/GPX/1/1";

        var doc = XDocument.Load(filename);
        var trackPoints = doc.Descendants(ns + "trkpt");

        Console.WriteLine($"There are {trackPoints.Count()} track points in {filename}");

        foreach (var trackPoint in trackPoints)
        {
            var wgsLat = Convert.ToDouble(trackPoint.Attribute("lat").Value);
            var wgsLon = Convert.ToDouble(trackPoint.Attribute("lon").Value);
            var gcjPoint = transformer.WGS2GCJ(wgsLat, wgsLon);

            trackPoint.Attribute("lat").Value = gcjPoint.Lat.ToString("F7");
            trackPoint.Attribute("lon").Value = gcjPoint.Lng.ToString("F7");
        }

        var newFile = File.CreateText(filename.Substring(0, filename.Length - 4) + "-gcj.gpx");
        doc.Save(newFile);

    }
}